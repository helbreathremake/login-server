USE [helbreath]
GO

/****** Object:  Table [dbo].[MarketOrders]    Script Date: 2021-04-14 20:36:58 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[MarketOrders](
	[ID] [uniqueidentifier] NOT NULL,
	[MerchantId] [int] NOT NULL,
	[ItemId] [int] NOT NULL,
	[Count] [int] NOT NULL DEFAULT 0,
	[Endurance] [int] NULL,
	[Colour] [int] NULL,
	[SpecialEffect1] [int] NULL,
	[SpecialEffect2] [int] NULL,
	[SpecialEffect3] [int] NULL,
	[Quality] [int] NULL,
	[Stats] [nvarchar](max) NULL,
	[Level] [int] NULL,
	[Unlimited] [bit] NOT NULL DEFAULT 0,
    [ColorType] [int] NULL,
	[CreatedOn] [datetime] NULL
	
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO

(id,merchantid,itemid,count,endurance,color,specialeffect1,specialeffect2,specialeffet3,quality,stats,level,unlimited,colortype)
(newid(),