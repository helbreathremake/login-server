﻿
using System.Windows.Forms;

namespace LoginServer
{
    public partial class ApplicationForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        private ListBox txtLoginLog;
        private TabControl tabControl1;
        private TabPage tabPage2;
        private TabPage tabPage1;
        private MenuStrip menuStrip1;
        private ToolStripMenuItem fileToolStripMenuItem;
        private ToolStripMenuItem exitToolStripMenuItem;
        private ListBox listAccounts;
        private GroupBox groupBox2;
        private ListBox listCharacters;
        private GroupBox groupBox1;
        private Label lblCreatedOn;
        private Label lblName;
        private SplitContainer splitContainer1;
        private SplitContainer splitContainer2;
        private Label lblCharacterChar;
        private Label lblCharacterMagic;
        private Label lblCharacterInt;
        private Label lblCharacterVit;
        private Label lblCharacterDex;
        private Label lblCharacterStrength;
        private Label lblCharacterName;
        private TabPage tabPage3;
        private GroupBox groupBox3;
        private Label lblStatus;
        private Label label11;
        private Timer timer1;
        private Label lblUpTime;
        private Label label4;
        private System.Windows.Forms.ToolStripMenuItem configurationToolStripMenuItem;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator1;
        private System.Windows.Forms.ToolStripMenuItem toolStripMenuItem1;
        private System.Windows.Forms.ToolStripMenuItem helpToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem aboutToolStripMenuItem;
    }
    
}

