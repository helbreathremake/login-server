USE [helbreath]
GO

/****** Object:  Table [dbo].[Accounts]    Script Date: 2021-04-14 20:08:31 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[Accounts](
	[ID] [uniqueidentifier] NOT NULL,
	[Name] [nvarchar](50) NULL,
	[Password] [nvarchar](max) NULL,
	[Email] [nvarchar](max) NULL,
	[CreatedOn] [datetime] NULL,
	[Question] [nvarchar](max) NULL,
	[Answer] [nvarchar](max) NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO


