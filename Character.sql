USE [helbreath]
GO

/****** Object:  Table [dbo].[Characters]    Script Date: 6/29/2021 20:44:41 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[Characters](
	[ID] [uniqueidentifier] NOT NULL,
	[AccountID] [uniqueidentifier] NOT NULL,
	[Name] [nvarchar](50) NOT NULL,
	[Profile] [nvarchar](max) NULL,
	[Level] [int] NOT NULL,
	[Strength] [int] NOT NULL,
	[Dexterity] [int] NOT NULL,
	[Vitality] [int] NOT NULL,
	[Magic] [int] NOT NULL,
	[Intelligence] [int] NOT NULL,
	[Charisma] [int] NOT NULL,
	[Experience] [bigint] NOT NULL,
	[Gender] [int] NOT NULL,
	[Skin] [int] NOT NULL,
	[HairStyle] [int] NOT NULL,
	[HairColour] [int] NOT NULL,
	[UnderwearColour] [int] NOT NULL,
	[Appearance1] [int] NOT NULL,
	[Appearance2] [int] NOT NULL,
	[Appearance3] [int] NOT NULL,
	[Appearance4] [int] NOT NULL,
	[AppearanceColour] [int] NOT NULL,
	[Town] [int] NOT NULL,
	[MapName] [nvarchar](50) NOT NULL,
	[MapX] [int] NOT NULL,
	[MapY] [int] NOT NULL,
	[LastLogin] [datetime] NOT NULL,
	[HP] [int] NOT NULL,
	[MP] [int] NOT NULL,
	[SP] [int] NOT NULL,
	[Hunger] [int] NOT NULL,
	[Criticals] [int] NOT NULL,
	[Majestics] [int] NOT NULL,
	[SpecialAbilityTime] [int] NOT NULL,
	[SkillStatus] [nvarchar](max) NOT NULL,
	[SpellStatus] [nvarchar](max) NOT NULL,
	[TotalLogins] [int] NOT NULL,
	[ReputationTime] [int] NOT NULL,
	[EnemyKills] [int] NOT NULL,
	[CriminalCount] [int] NOT NULL,
	[MuteTime] [int] NOT NULL,
	[Guild] [nvarchar](max) NULL,
	[GuildRank] [int] NOT NULL,
	[Contribution] [int] NOT NULL,
	[Reputation] [int] NOT NULL,
	[Gold] [int] NOT NULL,
	[RebirthLevel] [int] NOT NULL,
	[GladiatorPoints] [int] NOT NULL,
	[Type] [int] NOT NULL,
	[AdminLevel] [int] NOT NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO

ALTER TABLE [dbo].[Characters] ADD  DEFAULT ((0)) FOR [GuildRank]
GO

ALTER TABLE [dbo].[Characters] ADD  DEFAULT ((0)) FOR [Contribution]
GO

ALTER TABLE [dbo].[Characters] ADD  DEFAULT ((0)) FOR [Reputation]
GO

ALTER TABLE [dbo].[Characters] ADD  DEFAULT ((0)) FOR [Gold]
GO

ALTER TABLE [dbo].[Characters] ADD  DEFAULT ((0)) FOR [RebirthLevel]
GO

ALTER TABLE [dbo].[Characters] ADD  DEFAULT ((0)) FOR [GladiatorPoints]
GO

ALTER TABLE [dbo].[Characters] ADD  DEFAULT ((0)) FOR [Type]
GO

ALTER TABLE [dbo].[Characters] ADD  DEFAULT ((0)) FOR [AdminLevel]
GO


