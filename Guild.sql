USE [helbreath]
GO

/****** Object:  Table [dbo].[Guilds]    Script Date: 2021-04-14 20:36:58 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[Guilds](
	[ID] [uniqueidentifier] NOT NULL,
	[Name] [nvarchar](max) NOT NULL,
	[FoundedOn] [datetime] NULL,
	[FoundedBy] [uniqueidentifier] NOT NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO


