USE [helbreath]
GO

/****** Object:  Table [dbo].[CharacterTitles]    Script Date: 2021-04-14 20:36:58 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[CharacterTitles](
	[CharacterID] [uniqueidentifier] NULL,
	[DistanceTravelled] [int] not null default 0,
	[ItemsCollected]  [int] not null default 0,
	[RareItemsCollected]  [int] not null default 0,
	[MonstersKilled] [int] not null default 0,
	[PlayersKilled]  [int] not null default 0,
	[MonsterDeaths]  [int] not null default 0,
	[PlayerDeaths]  [int] not null default 0,
	[MonsterDamageDealt]  [int] not null default 0,
	[PlayerDamageDealt]  [int] not null default 0,
	[QuestsCompleted]  [int] not null default 0,
	[HighestMeleeDamage]  [int] not null default 0,
	[HighestMagicDamage]  [int] not null default 0,
	[HighestValueItem]  [int] not null default 0,
	[PublicEventsGold]  [int] not null default 0,
	[PublicEventsSilver]  [int] not null default 0,
	[PublicEventsBronze]  [int] not null default 0
	
) ON [PRIMARY]
GO


