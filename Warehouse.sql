USE [helbreath]
GO

/****** Object:  Table [dbo].[Warehouse]    Script Date: 2021-04-14 20:37:25 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[Warehouse](
	[ID] [uniqueidentifier] NOT NULL,
	[Index] [int] NOT NULL,
	[CharacterID] [uniqueidentifier] NULL,
	[BoundCharacterID] [uniqueidentifier] NULL,
	[ItemID] [int] NULL,
	[Count] [int] NULL,
	[Endurance] [int] NULL,
	[Colour] [int] NULL,
	[SpecialEffect1] [int] NULL,
	[SpecialEffect2] [int] NULL,
	[SpecialEffect3] [int] NULL,
	[BagPositionX] [int] NULL,
	[BagPositionY] [int] NULL,
	[Stats] [nvarchar](max) NULL,
	[IsEquipped] [int] NULL,
	[Quality] [int] NULL,
	[Level] [int] NULL,
	[SetNumber] [int] NULL,
	[ColorType] [int] NULL,
	[CreatedOn] [datetime] NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO


